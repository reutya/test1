import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User|null>
 


  constructor(public afAuth:AngularFireAuth,private router:Router) { 
    this.user = this.afAuth.authState;
  }


  // Signup(email:string, password:string){
  //   this.afAuth
  //       .auth
  //       .createUserWithEmailAndPassword(email,password)
  //       .then(res => 
  //         {
  //           console.log('Succesful sign up!!!!!!',res);
  //           this.router.navigate(['/suc']);
            
  //         }
  //       );
  // }


  doRegister(value){
    return new Promise<any>((resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
      .then(res => {
        resolve(res);
      }, err => reject(err))
    })
  }

Signup(email:string, password:string){
     this.afAuth
     .auth.createUserWithEmailAndPassword(email,password).then
      (res => { console.log('Succesful Signup', res);
       this.router.navigate(['/success']);
      }
       )
     .catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      //alert(errorMessage);
      console.log(error);
      console.log(errorCode);
      
     
    
    });
  }
 

  Logout(){
    this.afAuth.auth.signOut().then(res=>console.log('succses logout'))
  }

  
  // login(email:string, password:string){
  //   this.afAuth
  //       .auth.signInWithEmailAndPassword(email,password)
  //       .then(
  //         // res => console.log('Succesful Login',res)
  //        // res => console.log(this.user.subscribe(user=>console.log(user.uid)))
  //        user=>{this.router.navigate(['/suc'])}
  //       )
  // }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
          // res => console.log('Succesful Login',res)
         // res => console.log(this.user.subscribe(user=>console.log(user.uid)))
         user=>{this.router.navigate(['/suc'])})
         .catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          alert(errorMessage);
          console.log(error);
          console.log(errorCode);
          
         
        }
        )
     
  }


}

