// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA6IjRinVzlAXPy1TeUehUwTse8gOqOlvA",
  authDomain: "bbc1-1a761.firebaseapp.com",
  databaseURL: "https://bbc1-1a761.firebaseio.com",
  projectId: "bbc1-1a761",
  storageBucket: "bbc1-1a761.appspot.com",
  messagingSenderId: "604614500585",
  appId: "1:604614500585:web:6c4ec28d0b61a221d1d078",
  measurementId: "G-DJ1E2CDCRD"
  }  
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
